public class Matrimonial_Beds extends Single_Beds {

	private String style;
	private String madeOf;
	
	
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	
    public String getMadeOf() {
		return madeOf;
	}



   public void setMadeOf(String madeOf) {
		this.madeOf = madeOf;
	}

    public Matrimonial_Beds( String color, String type, int dimension, int height,int weight,String style, String madeOf) {
		    super(color,type,dimension,height,weight);
		    this.style = style;
		    this.madeOf = madeOf;
		
	}
    
     //same method here, but the size of the bed will increase
      public void CalculateSize() {
		
    	  System.out.println(getDimension()+getHeight()+getWeight());
    	  System.out.println("Matrimonial Bed it's made of "+ getMadeOf());
	}
	
}
