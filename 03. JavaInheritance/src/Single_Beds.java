
public class Single_Beds {

	
	private String color;
	private String type;
	private int dimension;
	private int height;
	private int weight;
	

	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getDimension() {
		return dimension;
	}
	public void setDimension(int dimension) {
		this.dimension = dimension;
	}
	
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public Single_Beds( String color, String type,int dimension ,int height, int weight) {
		super();
		this.color = color;
		this.type= type;
		this.dimension = dimension;
		this.height = height;
		this.weight = weight;
		
	}
	//we created a method CalculateSize which will calculate the size of the bed
	public void CalculateSize() {
		
		System.out.println(getDimension()+getHeight()+getWeight());
		System.out.println("The color of Single Bed is: " +getColor());
	}
}
