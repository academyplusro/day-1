package AbstractClass;

public class HotelEmployee extends Person {

private int empId = 0;
	
	public HotelEmployee(String name, String gender, int id) {
		super(name, gender);
		this.empId=id;
	}

	@Override
	public void work() {
		if(empId == 0){
			System.out.println("Working as hotel employee!!");
			
		}
	}
	
	
}
