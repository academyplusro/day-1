package AbstractClass;

public class Main {

	public static void main(String[] args) {
		Person student = new HotelEmployee("Dona","Female",0);
		Person employee = new HotelEmployee("Daniel","Male",1);
		student.work();
		employee.work();
		//using method implemented in abstract class - inheritance
		employee.changeName("Sebastian Odoherty");
		System.out.println(employee.toString());

	}

}
