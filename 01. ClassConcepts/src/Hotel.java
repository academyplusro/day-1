//the syntax required to define a class is:
public class Hotel {
 
	 //defining attributes - variables for instance
    float price;
    String name;
    String owner;
 
    //defining methods
    public float getPrice(){
        return price;
    }
    public String display(){
        return "The hotel "+name+" has the owner "+owner+ ".";
    }
	
}