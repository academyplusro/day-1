//objects construction,accessing attributes and methods
public class TestHotel {

	public static void main(String[] args) {
		//defining references
		Hotel hotel1;	//has null value
		Hotel hotel2 = null;
		//creating the object
		hotel1 = new Hotel();
		/*The object has access to its attributes and methods
		 *  (which are not static) through the operator "."		
		 */
		hotel1.price = 23;
		hotel1.name = "Pacific Phoenix Hotele";
        hotel1.owner = "Edward Walter";
	    System.out.println(hotel1.display());
	    //creating another object
        hotel2 = new Hotel();
	    hotel2.price = 35;
	    hotel2.name = "Ivory Manor Hotel";
	    hotel2.owner = "Christopher J. Nassetta";
        System.out.println(hotel2.display());

	}

}
